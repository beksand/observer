package pl.sda.designpatterns.observer.zad2;

public class Main {
    public static void main(String[] args) {
        ChatRoom chatRoom = new ChatRoom("room");
        chatRoom.addUser("Alex");
        chatRoom.addUser("Wiki");
        chatRoom.addUser("Bro");

        chatRoom.sendMessage(2, "Hej bro");
    }
}
