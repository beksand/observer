package pl.sda.designpatterns.observer.zad2;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ChatUser implements Observer {
    private int id;
    private String nick;
    private List<String> messages = new ArrayList<String>();
    private  boolean isAdmin;

    public ChatUser(int id, String nick) {
        this.id = id;
        this.nick = nick;

    }
    @Override
    public void update(Observable observable, Object receivedMessage) {
        if (receivedMessage instanceof Message) {
            Message msg = (Message) receivedMessage;
            if (msg.getSenderId()!=this.id){
                System.out.println(nick+" otrzymal/a "+receivedMessage);
                messages.add(msg.getMessage());
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public List<String> getMasseges() {
        return messages;
    }

    public void setMasseges(List<String> masseges) {
        this.messages = masseges;
    }

    public boolean isAdmin(boolean b) {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
