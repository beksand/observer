package pl.sda.designpatterns.observer.zad2;



import java.util.*;

public class ChatRoom extends Observable{
    private int userCounter=0;
    private List<String> adminsList = new ArrayList<String>(Arrays.asList("admin"));

    private Map<Integer, ChatUser> userMap = new HashMap<Integer, ChatUser>();
    private String roomName;

    public ChatRoom(String roomName) {

        this.roomName = roomName;

    }

    public void addUser(String nick){
        ChatUser newUser = new ChatUser(userCounter++, nick);

        for (ChatUser user : userMap.values()) {
            if (user.getNick().equals(newUser.getNick())) {
                System.out.println("User with that nick already exists");
                return;
            }
        }

        if (adminsList.contains(newUser.getNick())){
            newUser.isAdmin(true);
        }
        addObserver(newUser);
        userMap.put(newUser.getId(), newUser);
    }

    public void userLogin(String nick){

    }
    public void sendMessage(int id, String message){
        Message newMessage = new Message(id, message);
        setChanged();
        notifyObservers(newMessage);

    }
    public void kickUser(int id1, int id2){

    }
}
